# Introduction

Introduce ourselves and give a thesis statement about linux sandboxing being necessary but currently lacking and difficult.

Kevin

Midi

# Quick history of Unix security approaches

# What is Sandboxing?

# Why is sandboxing important?

* The pitfalls of blacklist-based security (e.g. antivirus)
* The pitfalls of whitelist-based security
* Security through compartmentalization
    * You already use sandboxing via your browser and phone. Why not extend it to your desktop?

# Sandboxes

## Not all sandboxes are security oriented

# Types

## Virtual Machines

* Virtualization via CPU extensions
    + Fast
    - Hypervistors generally require privileged (root)
    - Requires hardware support
* Emulation
    - Very very slow
    + Can run entirely unprivileged

Examples:
* KVM (via libvirt)
* QEMU
* Virtualbox
* VMWare
* Xen, Qubes (not technically linux)

## Containers

Containers don't exist. 

They are a word used to describe a collection of technologies that together can, but not necessarily always, provide a secure sandbox.

### Chroot

### seccomp

### cgroups

# Conclusion and Teaser for project


