# Linux Sandboxing

![](images/penguin-using-sandy-computer.png)

<aside class="notes">
Kevin with Thesis

- This talk does not cover:
  * Proprietary solutions (at least not too many)
  * Other operating systems (in depth)
  * Other security tools, such as:
    + Firewalls
    + Anti-virus
    + Mandatory Access Control, ie:
      - SELinux
      - AppArmor
    + Etc  
</aside>

## Introduction

| Ken | Midi |
|-----|------|
| Software dev @ ShorebreakSecurity.com | CEO / Software Dev / Consultant <br> @ Vojes Enterprises |
| Python, JS, Go, C#, Java | Py, C/C++, Java, Ada, Forth, Haskell, Lisp, Erlang/Elixir, Rust, Zig, and much more :D |
| https://VoidNet.tech<br>https://mstdn.party/kevinf | https://gitlab.com/cvoges12 |

# What is sandboxing?

Sandboxing gives each program or set of programs their own isolated environment.

Sandboxed programs have reduced access/insight into one's system

<aside class="notes">
Kevin

Uses:

- security
- testing and development
- recovery
- dependency control
- compatibility or legacy ABI
</aside>

## What is sandboxing?

![](https://i.stack.imgur.com/BfF5o.gif)

<aside class="notes">
Kevin
</aside>

## What is sandboxing?

![](images/cat-sandbox.png)

<aside class="notes">
Kevin
</aside>


# Why Sandboxing?

<aside class="notes">
Kevin
</aside>

## Before Sandboxing

* Pre-1960s
* Single-user systems
* Programs had substantial or entire control over the whole OS
* Installing software was not convenient or simple
* Smaller attack surface

<aside class="notes">
Kevin
</aside>

## Today

* Multi-user systems
* Multi-processing systems
* Internet is wide-spread
* Software can't be trusted
* Installing software happens often
* Large attack surface
* You use sandboxing everyday (IOS, Android, ChromeOS, browsers)

<aside class="notes">
Kevin

- Sandboxing is why said platforms don't need AV
</aside>

# Chroot

<aside class="notes">
Midi
</aside>

## Chroot

* 1979 - Version 7 Unix
* Stands for "change-root"
* Only limits file system access
* No isolation of memory, cpu, networking, etc

![](images/chroot-command.jpg)

<aside class="notes">
Midi
</aside>

# Jails

<aside class="notes">
Midi
</aside>

## FreeBSD Jails

* 2000 - FreeBSD `jail()` kernel syscall
* Not available on Linux
* Limited access to file system, processes, network stack, mount table
* By default, no raw socket access
* Isolated users and superuser accounts
* Shared kernel

<aside class="notes">
Midi
</aside>

## Firejail

* SUID app
* Uses Linux namespaces and `seccomp-bpf()`
  * available in all Linux kernels v3.x or higher
* Limited access to file system, processes, network stack, mount table
* Shared kernel
* Essentially FreeBSD jails on Linux

<aside class="notes">
Kevin
</aside>

## Bubblewrap and Bubblejail

* Bubblewrap is similar to firejail
* Not SUID app
* Bubblewrap has no premade profiles
* Bubblejail is a wrapper around bubblewrap with application profiles
* Also, flatpak uses bubblewrap

<aside class="notes">
Kevin
</aside>

# System Virtual Machines

<aside class="notes">
Midi
</aside>

## What?

Most commonly known as just "Virtual Machines" or "VMs"

* 1960s - IBM (CP-40)

![](images/vm-simple.ppm)

<aside class="notes">
Midi
</aside>

## Types of Virtual Machines

![](images/Hyperviseur.png)

<aside class="notes">
Midi
</aside>

## Type 2

![](images/hypervisor-type2.png)

<aside class="notes">
Midi
</aside>

## Type 2 Pros / Cons

* Pros
  * Convenience
  * Easy
* Cons
  * Performance
  * Security

<aside class="notes">
Midi
</aside>

## Type 2 Choices

* VirtualBox
* VMWare Workstation (proprietary)

<aside class="notes">
Midi
</aside>

## Type 1

![](images/hypervisor-type1.png)

<aside class="notes">
Midi
</aside>

## Type 1 Pros / Cons

* Pros
  * Performance
  * Security
* Cons
  * Not convenience
  * Not easy

<aside class="notes">
Midi
</aside>

## Type 1 Choices

* Xen:
  - Qubes OS
* KVM and KVM based solutions:
  - GNOME box
  - oVirt
  - QEMU/KVM

<aside class="notes">
Kevin
</aside>

## KVM and QEMU

![](images/qemu-infographic.png)

<aside class="notes">
Kevin
</aside>

# Qubes OS

* Best default desktop linux sandboxing experience
* Efficient, seamless & secure (for x11)
* Fair learning curve
* Overhead
* GPU hell

<aside class="notes">
Kevin
</aside>

## Qubes OS

![](images/qubes-desktop.jpg)

<aside class="notes">
Kevin
</aside>

# Process VM

* Unrelated to System VMs
* Stuff like JVM, BEAM, etc

<aside class="notes">
Midi
</aside>

# Containers

<aside class="notes">
Midi
</aside>

## Containers

![](images/containers.jpg)

<aside class="notes">
Midi
</aside>

## Containerd

![](images/containerd.png)

![](images/containerd-diagram.png)

<aside class="notes">
Midi
</aside>

## LXC / Docker

![](images/docker.png)

<aside class="notes">
Kevin
</aside>

## Podman

* Not mainly for security sandboxing
* `seccomp`, `cap-drop` can help
* Rootless by default

<aside class="notes">
Kevin
</aside>

## Snap

* Like flatpak, each package has its own sandbox
* AppArmor, seccomp, cgroup, etc

<aside class="notes">
Midi
</aside>

# Browser sandboxes

Browsers usually implement sandboxes through a series of process sandboxes and IPC brokers

<aside class="notes">
Midi
</aside>

# Android

* Unique UID per app
* SELinux, `seccomp`, and limited filesystem

<aside class="notes">
Midi
</aside>

# Chrome OS sandbox

* Chrome, Android, and Linux apps each have their own VM
* Similar to Android sandboxing
* Inside the Chrome VM, minijail is used to sandbox applications. (seccomp, namespaces, capabilities)
* Arguably most secure consumer OS, comparable to IOS

<aside class="notes">
Kevin
</aside>

# Conclusion

Slides availble at:

*https://gitlab.com/cvoges12/sandbox-talk*

**Questions?**


<script src="plugin/notes/notes.js"></script>
<script>
    Reveal.initialize({
        plugins: [ RevealNotes ]
    });
</script>
